all:
	g++ -o winterwar ./winterwar.cpp
clean:
	rm -f *.o
	rm -f -r winterwar
install:
	mkdir -p $(DESTDIR)/usr/bin/
	install -m 0755 winterwar $(DESTDIR)/usr/bin/
uninstall:
	rm -f $(DESTDIR)/usr/bin/winterwar
